import os
import numpy as np
from progressbar import ProgressBar, Percentage, Bar

#from scipy.misc import imread
from imageio import imread
import tensorflow as tf


DATA_PATH = '/local/FlyingChairs_release/data/'
SPLIT_PATH = '/local/FlyingChairs_release/FlyingChairs_train_val.txt'

# https://www.tensorflow.org/tutorials/load_data/tf-records 
def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

# Values defined here: https://lmb.informatik.uni-freiburg.de/resources/datasets/FlyingChairs.en.html#flyingchairs
TRAIN = 1
VAL = 2

# https://stackoverflow.com/questions/28013200/reading-middlebury-flow-files-with-python-bytes-array-numpy
def open_flo_file(filename):
    with open(filename, 'rb') as f:
        magic = np.fromfile(f, np.float32, count=1)
        if 202021.25 != magic:
            print('Magic number incorrect. Invalid .flo file')
        else:
            w = np.fromfile(f, np.int32, count=1)
            h = np.fromfile(f, np.int32, count=1)
            data = np.fromfile(f, np.float32, count=2*w[0]*h[0])
            # Reshape data into 3D array (columns, rows, bands)
            return np.resize(data, (w[0], h[0], 2))
        
# Create a dictionary with features that may be relevant.
def sequence(number):   
    image_a_path = os.path.join(DATA_PATH, '%05d_img1.ppm' % (number))
    image_b_path = os.path.join(DATA_PATH, '%05d_img2.ppm' % (number))
    flow_path    = os.path.join(DATA_PATH, '%05d_flow.flo' % (number))

    image_a = imread(image_a_path)
    image_b = imread(image_b_path)

    # Scale from [0, 255] -> [0.0, 1.0]
    image_a = image_a / 255.0
    image_b = image_b / 255.0

    image_a = np.float32(image_a)
    image_b = np.float32(image_b)
    
    image_a_raw = image_a.tostring()
    image_b_raw = image_b.tostring()
    flow_raw = open_flo_file(flow_path).tostring()

    image_shape = image_a.shape
    #print(image_shape)
    #print(image_shape[0])
    #scipy.misc.imsave('test_image_a.jpg', image_a)
    
    feature = {
        'height': _int64_feature(image_shape[0]),
        'width': _int64_feature(image_shape[1]),
        'depth': _int64_feature(image_shape[2]),
        'image_a': _bytes_feature(image_a_raw),
        'image_b': _bytes_feature(image_b_raw),
        'flow': _bytes_feature(flow_raw),
    }

    return tf.train.Example(features=tf.train.Features(feature=feature))

    
# Load train/val split into arrays
train_val_split = np.loadtxt(SPLIT_PATH)
train_idxs = np.flatnonzero(train_val_split == TRAIN)
val_idxs = np.flatnonzero(train_val_split == VAL)

# Convert the train and val datasets into .tfrecords format
#convert_dataset(train_idxs, 'fc_train')
#convert_dataset(val_idxs, 'fc_val')

#train_idxs = train_idxs[10000:]
#train_idxs = train_idxs[5000:10000]
#train_idxs = train_idxs[10008:]
#train_idxs = train_idxs[15000:20000]
#train_idxs = train_idxs[20000:]
#print(train_idxs)

vali_idxs = val_idxs[:]

with tf.python_io.TFRecordWriter('validation.tfrecords') as writer:
    count = 0
    pbar = ProgressBar(widgets=[Percentage(), Bar()], maxval=len(vali_idxs)).start()
    for i in vali_idxs: 
        tf_seq= sequence(i+1)
        writer.write(tf_seq.SerializeToString())
        pbar.update(count + 1)
        count += 1

# -*- coding: utf-8 -*-
"""
Created on Tue Jul 10 12:26:21 2018

@author: minak
"""

import tensorflow as tf

class Context_network(object):

    def __init__(self, name = 'context_net'):
        self.name = name
        
    def __call__(self, flow, feature):
        with tf.variable_scope(self.name):
            # stack estimated flow field and the last layer input of the flow estimator 
            x = tf.concat([flow, feature], axis = 3)

            x = tf.layers.conv2d(x, 128, 3, 1, 'same', dilation_rate= 1)
            x = tf.nn.leaky_relu(x, alpha=0.1)  
            x = tf.layers.conv2d(x, 128, 3, 1, 'same', dilation_rate= 2)
            x = tf.nn.leaky_relu(x, alpha=0.1)
            x = tf.layers.conv2d(x, 128, 3, 1, 'same', dilation_rate= 4)
            x = tf.nn.leaky_relu(x, alpha=0.1)
            x = tf.layers.conv2d(x,  96, 3, 1, 'same', dilation_rate= 8)
            x = tf.nn.leaky_relu(x, alpha=0.1)
            x = tf.layers.conv2d(x,  64, 3, 1, 'same', dilation_rate=16)
            x = tf.nn.leaky_relu(x, alpha=0.1)
            x = tf.layers.conv2d(x,  32, 3, 1, 'same', dilation_rate= 1)
            x = tf.nn.leaky_relu(x, alpha=0.1)
            x = tf.layers.conv2d(x,   2, 3, 1, 'same', dilation_rate= 1) # flow refinement increment

            return flow + x

# -*- coding: utf-8 -*-
"""
Created on Tue Jul 10 11:46:13 2018

@author: minak
"""
import tensorflow as tf

class FeaturePyramid_net(object):
    
    def __init__(self, num_levels, name = 'pyramid_feature_net'):
        self.num_levels = num_levels
        self.name = name
        self.f=[16, 32, 64, 96, 128, 192] # number of filters per level 
        
    def __call__(self, x, reuse = False):
        with tf.variable_scope(self.name, reuse=reuse):
            feature_pyramid = []
            for lvl in range(self.num_levels):
                # tf.layers.conv2d(inputs, filters, kernel_size, strides=(1, 1),padding='same')
                x = tf.layers.conv2d(x, self.f[lvl], 3, 2, 'same')
                x = tf.nn.leaky_relu(x, alpha=0.1)
                x = tf.layers.conv2d(x, self.f[lvl], 3, 1, 'same')
                x = tf.nn.leaky_relu(x, alpha=0.1)
                x = tf.layers.conv2d(x, self.f[lvl], 3, 1, 'same')
                x = tf.nn.leaky_relu(x, alpha=0.1)
                feature_pyramid.append(x)
                
        return feature_pyramid[::-1] # return in reveresed order (coarse -> fine)

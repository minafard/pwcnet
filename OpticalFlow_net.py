# -*- coding: utf-8 -*-
"""
Created on Tue Jul 10 12:26:21 2018

@author: minak
"""
import tensorflow as tf

class OpticalFlow_net(object):
    def __init__(self, name = 'OpticalFlow_net'):
        self.name = name
        self.channels=[275,211,179,147,115,99] 
        
    def __call__(self, features, cost, flow):
        with tf.variable_scope(self.name):

            # stack result correlation layer (cost volume), features, and upsampled flow
            x = tf.concat([cost, features, flow], axis = 3)

            # flow estimator network
            x = tf.layers.conv2d(x, 128, 3, 1, 'same')
            x = tf.nn.leaky_relu(x, alpha=0.1)
            x = tf.layers.conv2d(x, 128, 3, 1, 'same')
            x = tf.nn.leaky_relu(x, alpha=0.1)
            x = tf.layers.conv2d(x, 96, 3, 1, 'same')
            x = tf.nn.leaky_relu(x, alpha=0.1)
            x = tf.layers.conv2d(x, 64, 3, 1, 'same')
            x = tf.nn.leaky_relu(x, alpha=0.1)
            x = tf.layers.conv2d(x, 32, 3, 1, 'same')
            x = tf.nn.leaky_relu(x, alpha=0.1)
            flow = tf.layers.conv2d(x, 2, 3, 1, 'same')

            return x, flow 

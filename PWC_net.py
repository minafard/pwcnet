import tensorflow as tf
from OpticalFlow_net import OpticalFlow_net
from Context_network import Context_network
from FeaturePyramid_net import FeaturePyramid_net
from CostVolumeLayer import CostVolumeLayer
from Warping import Warping

class PWC_net(object):

        output_level = 4
        
        
        def __init__(self, max_displacement = 4,  name = 'pwc_net'):
            self.name = name
            
            # max displacment for correlation layer
            self.max_displacement = max_displacement
            
            # number of layers 
            self.num_levels = 6
            
            # output level (level at which the network only uses a upsampling to obtain the full resolution flow)
            self.output_level = 4
            
            # feature pyramid network 
            self.pyramid_feature_net = FeaturePyramid_net(self.num_levels)
            
            # warping layer 
            self.warp_layer = Warping()
            
            # corelation layer 
            self.cost_volume_layer = CostVolumeLayer(self.max_displacement)
            
            # optical flow estimator networks (one per level)
            self.OpticalFlow_net = [OpticalFlow_net(name = 'optflow_{}'.format(l)) for l in range(self.num_levels)]
            
            # flow refinement network (context network)
            self.context_nets = [Context_network(name ='context_{}'.format(l)) for l in range(self.num_levels)]

        def __call__(self, frame_0, frame_1):
            with tf.variable_scope(self.name):
                # TODO
                frame_0.set_shape([8,192,256,3])
                frame_1.set_shape([8,192,256,3])
                    
                # feature pyramid network (siamse architecture)
                pyramid_0 = self.pyramid_feature_net(frame_0)
                pyramid_1 = self.pyramid_feature_net(frame_1, reuse=True)
            
                # list holding the flow fields (including intermediate levels)
                flows = []
            
                # coarse to fine processing
                for l, (feature_0, feature_1) in enumerate(zip(pyramid_0, pyramid_1)):
                    print('Level {}'.format(l))
                
                    b, h, w, _ = tf.unstack(tf.shape(feature_0))

                    # upsample / init flow on coarsest level 
                    if l == 0:
                        flow = tf.zeros([b, h, w, 2], tf.float32) # init with zeros
                    else:
                        flow = tf.image.resize_bilinear(flow, (h, w)) # upsample
                    
                    # warp features towards refence view
                    flow_scale = tf.to_float(h) / 192.0 * 20.0 # correction factor (assuming gt is scaled by /20)
                    feature_1_warped = self.warp_layer(feature_1, flow*flow_scale)
                        
                    # compute costvolume with feature0 and warped feature1
                    cost = self.cost_volume_layer(feature_0, feature_1_warped)
                    
                    # estimated flow field 
                    feature, flow = self.OpticalFlow_net[l](feature_0, cost, flow)
    
                    # context net
                    flow = self.context_nets[l](flow, feature)

                    # append the flow field of each level (to apply loss per level)
                    flows.append(flow)
                    
                    # stop process if we reach the output level
                    if l == self.output_level:
                        # resize to original input size and exit level loop
                        finalflow = tf.image.resize_bilinear(flow, (192,256))
                        break 

                return finalflow, flows
            

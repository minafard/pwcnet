# PWCNet #

CNNs for Optical Flow Using Pyramid, Warping, and Cost Volume. This repository presents a TensorFlow-based implementation of 
"PWC-Net" by Deqing Sun et al. (CVPR 2018).

This README would normally document whatever steps are necessary to get your application up and running.

In order to get the idea of PWC-Net read the paper, or look at the PWC-Net presentation. 
The presentation consists of related works and present each componenet of PWC-Net.

### In order to able to run the project: ###

1. install the requirments.txt into your virtural environment.

2. Download the FlyingChair dataset from:
https://lmb.informatik.uni-freiburg.de/data/FlyingChairs/FlyingChairs.zip

3. Run the Chair_to_tfrecords.py into to create the tfrecord files
 from the dataset(TFrecords are faster to read and modify).If you only
want to try the model, you can try the premade flying_chairs_train_100.tfrecords
and skip this part.

4. Run the train_tfdata.py in order to run the model.

This is a sample input and output:


![Alt text](images/frame1.png "frame 1")
![Alt text](images/frame2.png "frame 2")
![Alt text](images/gt.png "groud truth")
![Alt text](images/of.png "optical flow")
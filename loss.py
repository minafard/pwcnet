# -*- coding: utf-8 -*-
"""
Created on Sat Jul 21 00:39:34 2018

@author: minak
"""

import tensorflow as tf
import numpy as np


class loss(object):
    def __init__(self,name='loss'):
        self.name=name

    def __call__(self,w_pyramid,w_gt,alpha,epsilon,q,loss_type):
        with tf.name_scope(self.name) as ns:
            # init with zero 
            weighted_loss = 0.0
            
            # loop over all the different resolution levels 
            for l, (alpha, flow) in enumerate(zip(alpha, w_pyramid)):
                
                #resize ground truth flow field to the predicted pyramid level size 
                _, h, w, _ = tf.unstack(tf.shape(flow))
                #flow_gt=tf.image.resize_bilinear(w_gt, (h, w))
                flow_gt=tf.image.resize_nearest_neighbor(w_gt, (h, w))

                if loss_type=='robust_training_loss':
                    loss = tf.reduce_mean(tf.reduce_sum(tf.norm(flow_gt-flow,ord=2,axis=3),axis=(1,2)))
                    weighted_loss = weighted_loss + (alpha*loss+epsilon)**q
                else: # loss_type=='multi_scale_loss'
                    loss = tf.reduce_mean(tf.reduce_sum(tf.norm(flow_gt-flow,ord=2,axis=3),axis=(1,2)))
                    weighted_loss = weighted_loss + alpha*loss
                    
            return  weighted_loss
        
    # end point error
    def EPE_loss(self, w_gt, flow):
        with tf.name_scope(self.name) as ns:
            epe = tf.reduce_mean(tf.norm(w_gt-flow, ord=2, axis=3))
        return epe   
    
    

import tensorflow as tf
import os
from PWC_net import PWC_net
from loss import loss
from flowlib import flow_to_image ,max_rad_finder,fl_to_idntcl_scl_img
#from tensorflow.python import debug as tf_debug

os.environ['TF_CPP_MIN_LOG_LEVEL']='3'
tf.logging.set_verbosity(tf.logging.INFO)


# tfrecords parser function
def _parse_(serialized):
    features = tf.parse_single_example(serialized,
        { 'height': tf.FixedLenFeature([], tf.int64),
          'width':  tf.FixedLenFeature([], tf.int64),
          'depth':  tf.FixedLenFeature([], tf.int64),
          'image_a':tf.FixedLenFeature([],tf.string),
          'image_b':tf.FixedLenFeature([],tf.string),
          'flow':   tf.FixedLenFeature([],tf.string)})
    
    # read dimension (depth for the flow is 2 !)
    height  = tf.cast(features['height'], tf.int32)
    width   = tf.cast(features['width'],  tf.int32)
    depth   = tf.cast(features['depth'],  tf.int32)
    
    # decode 
    image_a = tf.decode_raw(features['image_a'], tf.float32) 
    image_b = tf.decode_raw(features['image_b'], tf.float32)
    flow    = tf.decode_raw(features['flow'],    tf.float32)

    # reshape to actual size 
    image_a = tf.reshape(image_a, [height, width, depth])
    image_b = tf.reshape(image_b, [height, width, depth])
    flow    = tf.reshape(flow,    [height, width, 2])
    
    # crop to desired size and scale flow (according to flownet/pwc paper)
    image_a = tf.image.crop_to_bounding_box(image_a, 0, 0, 192, 256)
    image_b = tf.image.crop_to_bounding_box(image_b, 0, 0, 192, 256)
    flow = tf.image.crop_to_bounding_box(flow, 0, 0, 192, 256)
    flow /= 20.0

    return {"image_a": image_a, "image_b": image_b}, flow

def train_input_fn():
    filenames = ["flying_chairs_train_100.tfrecords"]
    # with tf.device('/cpu:0'):
    dataset = tf.data.TFRecordDataset(filenames)
    dataset = dataset.map(map_func=_parse_, num_parallel_calls=8)
    dataset = dataset.shuffle(buffer_size=1024)
    dataset = dataset.repeat()  # Repeat the input indefinitely.
    # TODO replace with shuffle_and_repeat
    dataset = dataset.batch(8)
    dataset = dataset.prefetch(1) # check if more help

    return dataset

def model_fn(features, labels, mode): # TODO add params
    model = PWC_net(4) 
    finalflow, flows = model(features["image_a"], features["image_b"])
    
    # If prediction mode, early return
    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode, predictions=finalflow)

    flowgt = labels
    
    # parameters [TODO]
    cost = loss()
    alphas = [0.32, 0.08, 0.02, 0.01, 0.005]
    eps = 0.02
    q = 0.4 
    loss_type = 'multi_scale_loss'
    gamma = 0.0004
    lr = 0.0001
    
    # loss function for training 
    flow_loss = cost(flows, flowgt, alphas, eps, q, loss_type)
    weight_loss = tf.reduce_sum([tf.nn.l2_loss(var) for var in tf.trainable_variables()])
    total_loss = flow_loss + gamma * weight_loss 
    
    # additional aee loss
    epe = cost.EPE_loss(20.0*flowgt, 20.0*finalflow)
    
    # add some stuff for the tensorboard summary
    tf.summary.scalar('AEE', epe)
    tf.summary.scalar('Total_Loss', total_loss)
    tf.summary.scalar('Weight_Loss', weight_loss)
    tf.summary.scalar('Flow_Loss', flow_loss)
    
    tf.summary.image("image_a", features["image_a"], max_outputs=2)
    tf.summary.image("image_b", features["image_b"], max_outputs=2)
    
    flowgt_image_0 = flowgt[0, :, :, :]
#    flowgt_image_0 = tf.py_func(flow_to_image, [flowgt_image_0], tf.uint8)
    flowgt_image_1 = flowgt[1, :, :, :]
#    flowgt_image_1 = tf.py_func(flow_to_image, [flowgt_image_1], tf.uint8)
#    flowgt_image_img = tf.stack([flowgt_image_0, flowgt_image_1], 0)
#    tf.summary.image('Ground_Truth_Flow', flowgt_image_img, max_outputs=2)
#    
    finalflow_image_0 = finalflow[0, :, :, :]
#    finalflow_image_0 = tf.py_func(flow_to_image, [finalflow_image_0], tf.uint8)
    finalflow_image_1 = finalflow[1, :, :, :]
#    finalflow_image_1 = tf.py_func(flow_to_image, [finalflow_image_1], tf.uint8)
#    finalflow_image_img = tf.stack([finalflow_image_0, finalflow_image_1], 0)
#    tf.summary.image('Computed_Flow', finalflow_image_img, max_outputs=2)
    
    
    maxrad_1=tf.py_func(max_rad_finder,[flowgt_image_1,finalflow_image_1],tf.uint8)
    maxrad_0=tf.py_func(max_rad_finder,[flowgt_image_0,finalflow_image_0],tf.uint8)
    
    scl_flowgt_image_0 = tf.py_func(fl_to_idntcl_scl_img,[flowgt_image_0,maxrad_0],tf.uint8)
    scl_flowgt_image_1 = tf.py_func(fl_to_idntcl_scl_img,[flowgt_image_1,maxrad_1],tf.uint8)
    flowgt_image_img_scl=tf.stack([scl_flowgt_image_0, scl_flowgt_image_1], 0)
    tf.summary.image('Ground_truth_identical_scale',flowgt_image_img_scl,max_outputs=2)

    scl_finalflow_image_0=tf.py_func(fl_to_idntcl_scl_img,[finalflow_image_0,maxrad_0],tf.uint8)
    scl_finalflow_image_1=tf.py_func(fl_to_idntcl_scl_img,[finalflow_image_1,maxrad_1],tf.uint8)
    finalflow_image_img_scl=tf.stack([scl_finalflow_image_0,scl_finalflow_image_1],0)
    tf.summary.image('Computed_Flow_identical_scale', finalflow_image_img_scl, max_outputs=2)
    
    
    
    
    # learning rate according to iteration number
    global_step = tf.train.get_or_create_global_step() # get_global_step()
    boundaries = [400000, 600000, 800000, 1000000]
    values = [lr, lr/2.0, lr/4, lr/8, lr/16]
    learning_rate = tf.train.piecewise_constant(global_step, boundaries, values)
    
    #optimizer (default values : beta1 = 0.9 and beta2 = 0.999
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
    train_op = optimizer.minimize(total_loss,
                                  var_list = tf.trainable_variables(), 
                                  global_step=tf.train.get_global_step())
    
    logging_hook = tf.train.LoggingTensorHook({"total_loss" : total_loss, 
    "epe" : epe}, every_n_iter=10)
    
    # TF Estimators requires to return a EstimatorSpec, that specify
    # the different ops for training, evaluating, ...
    estim_specs = tf.estimator.EstimatorSpec(
        mode=mode,
        loss=total_loss,
        train_op=train_op,
        training_hooks = [logging_hook],
    )

    return estim_specs

class train(object):
    def __init__(self):  
        pass
    
    def __call__(self):
        
        model = tf.estimator.Estimator(model_fn,model_dir='log_all')

        # Train the Model
        model.train(train_input_fn, steps=20000)

if __name__=="__main__":
    training=train()
    training()
